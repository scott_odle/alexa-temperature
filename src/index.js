var APP_ID = undefined; // TODO: replace with 'amzn1.echo-sdk-ams.app.[your-unique-value-here]'

var aws = require('aws-sdk');
var strftime = require('strftime');
var AlexaSkill = require('./AlexaSkill');
var config = require('./config');

// Connection to DynamoDB to retrieve temperature data
var dynamo = new aws.DynamoDB({
  region: config.aws_region,
  accessKeyId: config.aws_key,
  secretAccessKey: config.aws_secret
});

// Create TemperatureSkill as a child of AlexaSkill
var TemperatureSkill = function () {
  AlexaSkill.call(this, APP_ID);
};
TemperatureSkill.prototype = Object.create(AlexaSkill.prototype);
TemperatureSkill.prototype.constructor = TemperatureSkill;

// Function to grab latest temperature value from DynamoDB and pass to a callback
// <callback> is a function with two parameter, the error and result objects returned from DynamoDB
function getLatestTemperature(callback) {
  dynamo.query({
    TableName: config.dynamodb_table_name,
    ExpressionAttributeValues: {
      ':thing_name': {
        S: config.thing_name
      }
    },
    KeyConditionExpression: 'device_name = :thing_name',
    Limit: 1,
    ScanIndexForward: false
  }, callback);
}

// Function to grab the oldest temperature reading since <time> from DynamoDB and pass it to a callback
// <time> is the number of milliseconds since midnight UTC on 1/1/1970.
// <callback> is a function with two parameters, the error and result objects returned from DynamoDB
function getEarliestTemperatureSince(time, callback) {
  dynamo.query({
    TableName: config.dynamodb_table_name,
    ExpressionAttributeValues: {
      ':thing_name': {
        S: config.thing_name
      },
      ':delta_timestamp': {
        S: '' + time
      }
    },
    ExpressionAttributeNames: {
      '#TS': 'timestamp'
    },
    KeyConditionExpression: 'device_name = :thing_name AND #TS >= :delta_timestamp',
    Limit: 1
  }, callback);
}

// Convert a DynamoDB row into a temperature reading object
function objectFromRow(row) {
  return {
    UTCTimestamp: parseInt(row.timestamp.S),
    PHXTimestamp: parseInt(row.timestamp.S) - (7 * 60 * 60 * 1000),
    fahrenheit: parseFloat(row.payload.M.state.M.reported.M.fahrenheit.N),
    celsius: parseFloat(row.payload.M.state.M.reported.M.celsius.N)
  };
}

function speakTemperature(response) {
  // Tell Alexa to speak. This also terminates the script.
  function speakText(text) {
    var output = {
      speech: text,
      type: AlexaSkill.speechOutputType.PLAIN_TEXT
    };
    response.tell(output);
  }
  
  getLatestTemperature(function(err, data) {
    if (err) console.log(err, err.stack);
    else {
      if (!data.Items.length)
        speakText("No recent temperature data is available. Please check your device's connectivity.");

      var currentValue = objectFromRow(data.Items[0]);

      if ((new Date() - currentValue.UTCTimestamp) > (60 * 60 * 1000))
        speakText("No recent temperature data is available. Please check your device's connectivity.");

      getEarliestTemperatureSince(currentValue.UTCTimestamp - (60 * 60 * 1000), function(err, earlierData) {
        if (err) console.log(err, err.stack);
        else {
          var friendlyCurrentValue = parseFloat(currentValue.fahrenheit.toFixed(1));
          var friendlyCurrentLocalTime = strftime('%I:%M %p', new Date(currentValue.PHXTimestamp));
          var temperatureSpeech = 'As of ' + friendlyCurrentLocalTime + ', the temperature is ' + friendlyCurrentValue + ' degrees Fahrenheit';

          if (earlierData.Items.length) {
            var earlierValue = objectFromRow(earlierData.Items[0]);
            var friendlyEarlierValue = parseFloat(earlierValue.fahrenheit.toFixed(1));
            if ((currentValue.UTCTimestamp - earlierValue.UTCTimestamp) <= (60 * 60 * 1000)) {
              var temperatureDelta = friendlyCurrentValue - friendlyEarlierValue;
              var friendlyDelta = (temperatureDelta == 0) ? 'no change' :
                                                            (temperatureDelta < 0) ? 'a decrease of ' + parseFloat(Math.abs(temperatureDelta).toFixed(1)) + ' degrees' :
                                                                                     'an increase of ' + parseFloat(Math.abs(temperatureDelta).toFixed(1)) + ' degrees';
              var friendlyEarlierLocalTime = strftime('%I:%M %p', new Date(earlierValue.PHXTimestamp));
              speakText(temperatureSpeech + ', ' + friendlyDelta + ' since ' + friendlyEarlierLocalTime);
            }
          }

          speakText(temperatureSpeech + '.');
        }
      });
    }
  });
}

// Alexa intents
TemperatureSkill.prototype.intentHandlers = {
  TemperatureIntent: function(intent, session, response) {
    speakTemperature(response);
  }
};

// Create the handler that responds to the Alexa Request.
exports.handler = function(event, context) {
    var tempSkill = new TemperatureSkill();
    tempSkill.execute(event, context);
};
